# Automated build of HA k3s Cluster with `kube-vip` and MetalLB

This playbook will build an HA Kubernetes cluster with `k3s`, `kube-vip` and MetalLB via `ansible`.

This is based on the work from [this fork](https://github.com/212850a/k3s-ansible) which is based on the work from [k3s-io/k3s-ansible](https://github.com/k3s-io/k3s-ansible). It uses [kube-vip](https://kube-vip.chipzoller.dev/) to create a load balancer for control plane, and [metal-lb](https://metallb.universe.tf/installation/) for its service `LoadBalancer`.

## 📖 k3s Ansible Playbook

Build a Kubernetes cluster using Ansible with k3s. The goal is easily install a HA Kubernetes cluster on machines running Ubuntu 22.04.

## ✅ System requirements

- Control Node (the machine you are running `ansible` commands) must have Ansible 2.11+. In our case this is k3sbastion server.

- You will also need to install collections that this playbook uses by running `ansible-galaxy collection install -r ./collections/requirements.yml` (important❗)

- [`netaddr` package](https://pypi.org/project/netaddr/) must be available to Ansible. If you have installed Ansible via apt, this is already taken care of. If you have installed Ansible via `pip`, make sure to install `netaddr` into the respective virtual environment.

- `server` and `agent` nodes should have passwordless SSH access, if not you can supply arguments to provide credentials `--ask-pass --ask-become-pass` to each command.

## 🚀 Getting Started

### 🍴 Preparation

Update the ansible inventory matching your environment. 
Edit `inventory/k3s-cluster/hosts.ini` to match your systems.

```bash
vi inventory/k3s-cluster/hosts.ini
```


For example:

```ini
[master]
10.10.10.20 ansible_ssh_private_key_file=/home/ansible/.ssh/id_rsa
10.10.10.23 ansible_ssh_private_key_file=/home/ansible/.ssh/id_rsa
10.10.10.21 ansible_ssh_private_key_file=/home/ansible/.ssh/id_rsa

[node]
10.10.10.22 ansible_ssh_private_key_file=/home/ansible/.ssh/id_rsa
10.10.10.24 ansible_ssh_private_key_file=/home/ansible/.ssh/id_rsa

[k3s_cluster:children]
master
node
```

If multiple hosts are in the master group, the playbook will automatically set up k3s in [HA mode with etcd](https://rancher.com/docs/k3s/latest/en/installation/ha-embedded/).

Finally, make sure that `ansible.cfg` is matching the location of the inventory file that you have updated.

This requires at least k3s version `1.19.1` however the version is configurable by using the `k3s_version` variable.

In case you deploy a new cluster also edit `inventory/k3s-cluster/group_vars/all.yml` to match your environment.
What are the important settings here :
```yaml
apiserver_endpoint: "10.10.10.201"
flannel_iface: ens34
metal_lb_ip_range: "10.10.10.100-10.10.10.110"
```
**apiserver_endpoint** : this is the apiserver_endpoint is virtual ip-address which will be configured on each master needed for HA
**flannel_iface**: this should be the primary interface of each VM. This is the same accross each VM in the cluster, but if you install a different version of the OS, it might be different (e.g. CentOS) - you can check this with the command "ip a sh"
**metal_lb_ip_range**: when you create a service object in k3s of type LoadBalancer, it will pick an ip-adress of this pool. Make sure that you never use these ip addresses for something else, otherwise you will have an IP address conflict.

### ☸️ Create Cluster

Start provisioning of the cluster using the following command:

```bash
ansible-playbook site.yml -i inventory/k3s-cluster/hosts.ini
```

After deployment control plane will be accessible via virtual ip-address which is defined in inventory/group_vars/all.yml as `apiserver_endpoint`

### 🔥 Remove k3s cluster

```bash
ansible-playbook reset.yml -i inventory/k3s-cluster/hosts.ini
```

>You should also reboot these nodes due to the VIP not being destroyed

## ⚙️ Kube Config

To copy your `kube config` locally so that you can access your **Kubernetes** cluster run:

```bash
scp ansible@k3sbastion:~/.kube/config ~/.kube/config
```

### 🔨 Testing your cluster

As ansible user run on k3sbastion :
```bash
kubectl get nodes
kubectl get pod -A
kubectl cluster-info
```
