apt update
apt install ansible git
ansible-galaxy collection install community.general
ansible-galaxy collection install ansible.posix
git clone https://github.com/jnidzwetzki/ansible-playbooks
cd ansible-playbooks
